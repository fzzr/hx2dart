package dart;

import dart.html.Window;
import dart.html.Element;

/*
    dart:html top level
 */

@:library("dart:html")
extern class Html
{
    public static inline function query(selector:String):Element return untyped __call_named__("query", selector);
    public static inline function queryAll(selector:String):Element return untyped __call_named__("queryAll", selector);

    static var window(get, null):Window; //dart.html.Window
    static inline function get_window():Window return untyped __dart__("window");

    static var document(get, null):Dynamic; //dart.html.Window
    static inline function get_document():Dynamic return untyped __dart__("document");
}

