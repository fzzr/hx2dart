import 'dart:html';
import 'dart:math';
 var TAU = PI * 2;
 var MAX_D = 300;
 var centerX = MAX_D / 2;
 var centerY = centerX;
 var slider;
 var notes;
 var PHI;
 var seeds = 0;
 var context;
 main (  ) {
		slider = query("#" + "slider");
		notes = query("#" + "notes");
		PHI = (sqrt(5) + 1) / 2;
		context = (query("#" + "canvas") as CanvasElement).getContext("2d");
		slider.onChange.listen(( e ) {
			draw();
		});
		draw();
		foo();
	}
 foo (  ) {
		return "boo";
	}
 draw (  ) {
		seeds = int.parse(slider.value);
		context.clearRect(0, 0, MAX_D, MAX_D);
		{
			var _g1 = 0, _g = seeds;
			while((_g1 < _g)) {
				var i = _g1++;
				var theta = i * TAU / PHI;
				var r = sqrt(i) * 4;
				drawSeed(centerX + r * cos(theta), centerY - r * sin(theta));
			};
		};
		notes.text = seeds.toString() + " seeds";
	}
 drawSeed ( x,y ) {
		context.beginPath();
		context.lineWidth = 2;
		context.fillStyle = "orange";
		context.strokeStyle = "orange";
		context.arc(x, y, 2, 0, TAU, false);
		context.fill();
		context.closePath();
		context.stroke();
	}
