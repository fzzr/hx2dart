package;
import js.Browser;

@:library("dart:html")//TEMP HACK as extern class metadata isn't currently being read
class Main
{
    public static function main()
    {
        var sampleText = Browser.document.getElementById("sample_text_id");
        sampleText.innerText = "Click me!";
        sampleText.onclick = reverseText;
    }

    @:toplevel //possible idea to mark static methods as dartlib toplevel methods
    static function reverseText(_)
    {
        var text = Browser.document.getElementById("sample_text_id").innerText;
        var buffer = new StringBuf();
        var i = text.length;
        while(i-- > 0)
        {
            buffer.add(text.charAt(i));
        }
        Browser.document.getElementById("sample_text_id").innerText = buffer.toString();
    }
}