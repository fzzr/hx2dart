(function () { "use strict";
var Ball = function(root,x,y,color) {
	this.root = root;
	this.x = x;
	this.y = y;
	this.elem = js.Browser.document.createElement("img");
	this.elem.src = Balls.PNGS[color];
	Clock.makeAbsolute(this.elem);
	Clock.setElementPosition(this.elem,x,y);
	root.appendChild(this.elem);
	this.ax = 0.0;
	this.ay = Ball.GRAVITY;
	this.vx = Ball.randomVelocity();
	this.vy = Ball.randomVelocity();
};
Ball.__name__ = true;
Ball.randomVelocity = function() {
	return (Math.random() - 0.5) * Ball.INIT_VELOCITY;
}
Ball.prototype = {
	tick: function(delta) {
		this.vx += this.ax * delta;
		this.vy += this.ay * delta;
		this.x += this.vx * delta;
		this.y += this.vy * delta;
		if(this.x < Ball.RADIUS || this.x > Balls.get_clientWidth()) {
			this.elem.remove();
			return false;
		}
		if(this.y > Balls.get_clientHeight()) {
			this.y = Balls.get_clientHeight();
			this.vy *= -Ball.RESTITUTION;
		}
		Clock.setElementPosition(this.elem,this.x - Ball.RADIUS,this.y - Ball.RADIUS);
		return true;
	}
}
var Balls = function() {
	this.lastTime = new Date().getTime();
	this.balls = [];
	this.root = js.Browser.document.createElement("div");
	js.Browser.document.body.appendChild(this.root);
	Clock.makeAbsolute(this.root);
	Clock.setElementSize(this.root,0.0,0.0,0.0,0.0);
};
Balls.__name__ = true;
Balls.get_clientWidth = function() {
	return js.Browser.window.innerWidth;
}
Balls.get_clientHeight = function() {
	return js.Browser.window.innerHeight;
}
Balls.prototype = {
	add: function(x,y,color) {
		this.balls.push(new Ball(this.root,x,y,color));
	}
	,newDistanceSquared: function(delta,b0,b1) {
		var nb0x = b0.x + b0.vx * delta;
		var nb0y = b0.y + b0.vy * delta;
		var nb1x = b1.x + b1.vx * delta;
		var nb1y = b1.y + b1.vy * delta;
		var ndx = Math.abs(nb0x - nb1x);
		var ndy = Math.abs(nb0y - nb1y);
		var nd2 = ndx * ndx + ndy * ndy;
		return nd2;
	}
	,collideBalls: function(delta) {
		var _g = this;
		Lambda.iter(this.balls,function(b0) {
			Lambda.iter(_g.balls,function(b1) {
				var dx = Math.abs(b0.x - b1.x);
				var dy = Math.abs(b0.y - b1.y);
				var d2 = dx * dx + dy * dy;
				if(d2 < Balls.RADIUS2) {
					if(_g.newDistanceSquared(delta,b0,b1) > d2) return;
					var d = Math.sqrt(d2);
					if(d == 0) return;
					dx /= d;
					dy /= d;
					var impactx = b0.vx - b1.vx;
					var impacty = b0.vy - b1.vy;
					var impactSpeed = impactx * dx + impacty * dy;
					b0.vx -= dx * impactSpeed;
					b0.vy -= dy * impactSpeed;
					b1.vx += dx * impactSpeed;
					b1.vy += dy * impactSpeed;
				}
			});
		});
	}
	,tick: function(now) {
		Clock.showFps(1000.0 / (now - this.lastTime + 0.01));
		var delta = Math.min((now - this.lastTime) / 1000.0,0.1);
		this.lastTime = now;
		this.balls = this.balls.filter(function(ball) {
			return ball.tick(delta);
		});
		this.collideBalls(delta);
	}
}
var Clock = function() { }
Clock.__name__ = true;
Clock.main = function() {
	new CountDownClock();
}
Clock.showFps = function(fps) {
	if(Clock.fpsAverage == null) Clock.fpsAverage = fps;
	Clock.fpsAverage = fps * 0.05 + Clock.fpsAverage * 0.95;
	js.Browser.document.getElementById("notes").innerText = Std.string(Math.round(Clock.fpsAverage)) + " fps";
}
Clock.makeAbsolute = function(elem) {
	elem.style.left = "0px";
	elem.style.top = "0px";
	elem.style.position = "absolute";
}
Clock.makeRelative = function(elem) {
	elem.style.position = "relative";
}
Clock.setElementPosition = function(elem,x,y) {
	elem.style.transform = "translate(" + Std.string(x) + "px, " + Std.string(y) + "px)";
}
Clock.setElementSize = function(elem,l,t,r,b) {
	Clock.setElementPosition(elem,l,t);
	elem.style.right = "${r}px";
	elem.style.bottom = "${b}px";
}
var CountDownClock = function() {
	this.hours = [null,null];
	this.minutes = [null,null];
	this.seconds = [null,null];
	this.displayedHour = this.displayedMinute = this.displayedSecond = -1;
	this.balls = new Balls();
	var parent = js.Browser.document.getElementById("canvas-content");
	this.createNumbers(parent,parent.clientWidth,parent.clientHeight);
	this.updateTime(new Date());
	js.Browser.window.requestAnimationFrame($bind(this,this.tick));
};
CountDownClock.__name__ = true;
CountDownClock.prototype = {
	createNumbers: function(parent,width,height) {
		var root = js.Browser.document.createElement("div");
		Clock.makeRelative(root);
		root.style.textAlign = "center";
		js.Browser.document.getElementById("canvas-content").appendChild(root);
		var hSize = (CountDownClock.BALL_WIDTH * ClockNumber.WIDTH + CountDownClock.NUMBER_SPACING) * 6 + (CountDownClock.BALL_WIDTH + CountDownClock.NUMBER_SPACING) * 2;
		hSize -= CountDownClock.NUMBER_SPACING;
		var vSize = CountDownClock.BALL_HEIGHT * ClockNumber.HEIGHT;
		var x = (width - hSize) / 2;
		var y = (height - vSize) / 3;
		var _g1 = 0, _g = this.hours.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.hours[i] = new ClockNumber(this,x,Balls.BLUE_BALL_INDEX);
			root.appendChild(this.hours[i].root);
			Clock.setElementPosition(this.hours[i].root,x,y);
			x += CountDownClock.BALL_WIDTH * ClockNumber.WIDTH + CountDownClock.NUMBER_SPACING;
		}
		root.appendChild(new Colon(x,y).root);
		x += CountDownClock.BALL_WIDTH + CountDownClock.NUMBER_SPACING;
		var _g1 = 0, _g = this.minutes.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.minutes[i] = new ClockNumber(this,x,Balls.RED_BALL_INDEX);
			root.appendChild(this.minutes[i].root);
			Clock.setElementPosition(this.minutes[i].root,x,y);
			x += CountDownClock.BALL_WIDTH * ClockNumber.WIDTH + CountDownClock.NUMBER_SPACING;
		}
		root.appendChild(new Colon(x,y).root);
		x += CountDownClock.BALL_WIDTH + CountDownClock.NUMBER_SPACING;
		var _g1 = 0, _g = this.seconds.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.seconds[i] = new ClockNumber(this,x,Balls.GREEN_BALL_INDEX);
			root.appendChild(this.seconds[i].root);
			Clock.setElementPosition(this.seconds[i].root,x,y);
			x += CountDownClock.BALL_WIDTH * ClockNumber.WIDTH + CountDownClock.NUMBER_SPACING;
		}
	}
	,pad2: function(number) {
		if(number < 10) return "0${number}";
		return "${number}";
	}
	,setDigits: function(digits,numbers) {
		var _g1 = 0, _g = numbers.length;
		while(_g1 < _g) {
			var i = _g1++;
			var digit = HxOverrides.cca(digits,i) - HxOverrides.cca("0",0);
			numbers[i].setPixels(ClockNumbers.PIXELS[digit]);
		}
	}
	,updateTime: function(now) {
		if(now.getHours() != this.displayedHour) {
			this.setDigits(this.pad2(now.getHours()),this.hours);
			this.displayedHour = now.getHours();
		}
		if(now.getMinutes() != this.displayedMinute) {
			this.setDigits(this.pad2(now.getMinutes()),this.minutes);
			this.displayedMinute = now.getMinutes();
		}
		if(now.getSeconds() != this.displayedSecond) {
			this.setDigits(this.pad2(now.getSeconds()),this.seconds);
			this.displayedSecond = now.getSeconds();
		}
	}
	,tick: function(time) {
		this.updateTime(new Date());
		this.balls.tick(time);
		js.Browser.window.requestAnimationFrame($bind(this,this.tick));
	}
}
var HxOverrides = function() { }
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
}
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
}
var Lambda = function() { }
Lambda.__name__ = true;
Lambda.iter = function(it,f) {
	var $it0 = $iterator(it)();
	while( $it0.hasNext() ) {
		var x = $it0.next();
		f(x);
	}
}
var ClockNumber = function(app,pos,ballColor) {
	this.app = app;
	this.ballColor = ballColor;
	this.imgs = [];
	var _g1 = 0, _g = ClockNumber.HEIGHT;
	while(_g1 < _g) {
		var i = _g1++;
		this.imgs.push(null);
	}
	this.root = js.Browser.document.createElement("div");
	Clock.makeAbsolute(this.root);
	Clock.setElementPosition(this.root,pos,0.0);
	var _g1 = 0, _g = ClockNumber.HEIGHT;
	while(_g1 < _g) {
		var y = _g1++;
		this.imgs[y] = [];
		var _g3 = 0, _g2 = ClockNumber.WIDTH;
		while(_g3 < _g2) {
			var i = _g3++;
			this.imgs[y].push(null);
		}
	}
	var _g1 = 0, _g = ClockNumber.HEIGHT;
	while(_g1 < _g) {
		var y = _g1++;
		var _g3 = 0, _g2 = ClockNumber.WIDTH;
		while(_g3 < _g2) {
			var x = _g3++;
			this.imgs[y][x] = js.Browser.document.createElement("img");
			this.root.appendChild(this.imgs[y][x]);
			Clock.makeAbsolute(this.imgs[y][x]);
			Clock.setElementPosition(this.imgs[y][x],x * CountDownClock.BALL_WIDTH,y * CountDownClock.BALL_HEIGHT);
		}
	}
};
ClockNumber.__name__ = true;
ClockNumber.prototype = {
	setPixels: function(px) {
		var _g4 = this;
		var _g1 = 0, _g = ClockNumber.HEIGHT;
		while(_g1 < _g) {
			var y = _g1++;
			var _g3 = 0, _g2 = ClockNumber.WIDTH;
			while(_g3 < _g2) {
				var x = _g3++;
				var img = [this.imgs[y][x]];
				if(this.pixels != null) {
					if(this.pixels[y][x] != 0 && px[y][x] == 0) haxe.Timer.delay((function(img) {
						return function() {
							var r = img[0].getBoundingClientRect();
							var absx = r.left;
							var absy = r.top;
							_g4.app.balls.add(absx,absy,_g4.ballColor);
						};
					})(img),0);
				}
				if(px[y][x] != 0) img[0].src = Balls.PNGS[this.ballColor]; else img[0].src = Balls.PNGS[6];
			}
		}
		this.pixels = px;
	}
}
var Colon = function(xpos,ypos) {
	this.root = js.Browser.document.createElement("div");
	Clock.makeAbsolute(this.root);
	Clock.setElementPosition(this.root,xpos,ypos);
	var dot = js.Browser.document.createElement("img");
	dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
	this.root.appendChild(dot);
	Clock.makeAbsolute(dot);
	Clock.setElementPosition(dot,0.0,2.0 * CountDownClock.BALL_HEIGHT);
	dot = js.Browser.document.createElement("img");
	dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
	this.root.appendChild(dot);
	Clock.makeAbsolute(dot);
	Clock.setElementPosition(dot,0.0,4.0 * CountDownClock.BALL_HEIGHT);
};
Colon.__name__ = true;
var ClockNumbers = function() { }
ClockNumbers.__name__ = true;
var Std = function() { }
Std.__name__ = true;
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
}
var haxe = {}
haxe.Timer = function(time_ms) {
	var me = this;
	this.id = setInterval(function() {
		me.run();
	},time_ms);
};
haxe.Timer.__name__ = true;
haxe.Timer.delay = function(f,time_ms) {
	var t = new haxe.Timer(time_ms);
	t.run = function() {
		t.stop();
		f();
	};
	return t;
}
haxe.Timer.prototype = {
	run: function() {
		console.log("run");
	}
	,stop: function() {
		if(this.id == null) return;
		clearInterval(this.id);
		this.id = null;
	}
}
var js = {}
js.Boot = function() { }
js.Boot.__name__ = true;
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2, _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i;
			var str = "[";
			s += "\t";
			var _g = 0;
			while(_g < l) {
				var i1 = _g++;
				str += (i1 > 0?",":"") + js.Boot.__string_rec(o[i1],s);
			}
			str += "]";
			return str;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) { ;
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
}
js.Browser = function() { }
js.Browser.__name__ = true;
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; };
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; };
Math.__name__ = ["Math"];
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i) {
	return isNaN(i);
};
String.__name__ = true;
Array.__name__ = true;
Date.__name__ = ["Date"];
Ball.GRAVITY = 400.0;
Ball.RESTITUTION = 0.8;
Ball.INIT_VELOCITY = 800.0;
Ball.RADIUS = 14.0;
Balls.RADIUS2 = Ball.RADIUS * Ball.RADIUS;
Balls.GREEN_BALL_INDEX = 1;
Balls.BLUE_BALL_INDEX = 2;
Balls.DK_GRAY_BALL_INDEX = 4;
Balls.RED_BALL_INDEX = 5;
Balls.PNGS = ["images/ball-d9d9d9.png","images/ball-009a49.png","images/ball-13acfa.png","images/ball-265897.png","images/ball-b6b4b5.png","images/ball-c0000b.png","images/ball-c9c9c9.png"];
CountDownClock.NUMBER_SPACING = 19.0;
CountDownClock.BALL_WIDTH = 19.0;
CountDownClock.BALL_HEIGHT = 19.0;
ClockNumber.WIDTH = 4;
ClockNumber.HEIGHT = 7;
ClockNumbers.PIXELS = [[[1,1,1,1],[1,0,0,1],[1,0,0,1],[1,0,0,1],[1,0,0,1],[1,0,0,1],[1,1,1,1]],[[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1]],[[1,1,1,1],[0,0,0,1],[0,0,0,1],[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,1,1,1]],[[1,1,1,1],[0,0,0,1],[0,0,0,1],[1,1,1,1],[0,0,0,1],[0,0,0,1],[1,1,1,1]],[[1,0,0,1],[1,0,0,1],[1,0,0,1],[1,1,1,1],[0,0,0,1],[0,0,0,1],[0,0,0,1]],[[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,1,1,1],[0,0,0,1],[0,0,0,1],[1,1,1,1]],[[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,1,1,1],[1,0,0,1],[1,0,0,1],[1,1,1,1]],[[1,1,1,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1]],[[1,1,1,1],[1,0,0,1],[1,0,0,1],[1,1,1,1],[1,0,0,1],[1,0,0,1],[1,1,1,1]],[[1,1,1,1],[1,0,0,1],[1,0,0,1],[1,1,1,1],[0,0,0,1],[0,0,0,1],[1,1,1,1]]];
js.Browser.window = typeof window != "undefined" ? window : null;
js.Browser.document = typeof window != "undefined" ? window.document : null;
Clock.main();
})();
