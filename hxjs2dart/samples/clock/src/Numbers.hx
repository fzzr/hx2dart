package ;

import haxe.Timer;
import js.html.DivElement;
import js.html.ImageElement;
import js.html.Element;
import Clock;
import js.Browser;

class ClockNumber
{
    public static var WIDTH:Int = 4;
    public static var HEIGHT:Int = 7;

    var app:CountDownClock;
    public var root:Element;
    var imgs:Array<Array<ImageElement>>;
    var pixels:Array<Array<Int>>;
    var ballColor:Int;

    public function new(app, pos:Float, ballColor)
    {
        this.app = app;
        this.ballColor = ballColor;
        imgs = [];//new Array<Array<ImageElement>>(HEIGHT);
        for(i in 0 ... HEIGHT) imgs.push(null);

        root = Browser.document.createDivElement();
        Clock.makeAbsolute(root);
        Clock.setElementPosition(root, pos, 0.0);

        for (y in 0 ... HEIGHT)
        {
            imgs[y] = []; //new Array<ImageElement>(WIDTH);
            for(i in 0 ... WIDTH) imgs[y].push(null);
        }

        for (y in 0 ... HEIGHT)
        {
            for (x in 0 ... WIDTH)
            {
                imgs[y][x] = Browser.document.createImageElement();
                root.appendChild(imgs[y][x]);
                Clock.makeAbsolute(imgs[y][x]);
                Clock.setElementPosition(imgs[y][x],
                x * CountDownClock.BALL_WIDTH, y * CountDownClock.BALL_HEIGHT);
            }
        }
    }

    public function setPixels(px:Array<Array<Int>>)
    {
        for (y in 0 ... HEIGHT)
        {
            for (x in 0 ... WIDTH)
            {
                var img:ImageElement = imgs[y][x];

                if (pixels != null)
                {
                    if ((pixels[y][x] != 0) && (px[y][x] == 0))
                    {
                        Timer.delay(function()
                        {
                            var r = img.getBoundingClientRect();
                            var absx:Float = r.left;
                            var absy:Float = r.top;

                            app.balls.add(absx, absy, ballColor);
                        }, 0);
                    }
                }

//                img.src = px[y][x] != 0 ? Balls.PNGS[ballColor] : Balls.PNGS[6];     //TODO(av) sort ternarys
                if(px[y][x] != 0)
                    img.src = Balls.PNGS[ballColor];
                else
                    img.src = Balls.PNGS[6];
            }
        }

        pixels = px;
    }
}

class Colon
{
    public var root:Element;

    public function new(xpos:Float, ypos:Float)
    {
        root = Browser.document.createDivElement();
        Clock.makeAbsolute(root);
        Clock.setElementPosition(root, xpos, ypos);

        var dot = Browser.document.createImageElement();
        dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
        root.appendChild(dot);
        Clock.makeAbsolute(dot);
        Clock.setElementPosition(dot, 0.0, 2.0 * CountDownClock.BALL_HEIGHT);

        dot = Browser.document.createImageElement();
        dot.src = Balls.PNGS[Balls.DK_GRAY_BALL_INDEX];
        root.appendChild(dot);
        Clock.makeAbsolute(dot);
        Clock.setElementPosition(dot, 0.0, 4.0 * CountDownClock.BALL_HEIGHT);
    }
}

class ClockNumbers {
    public static var PIXELS =
    [
        [
            [ 1, 1, 1, 1 ],
            [ 1, 0, 0, 1 ],
            [ 1, 0, 0, 1 ],
            [ 1, 0, 0, 1 ],
            [ 1, 0, 0, 1 ],
            [ 1, 0, 0, 1 ],
            [ 1, 1, 1, 1 ]
        ],
        [
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ],
            [ 0, 0, 0, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 0 ],
        [ 1, 0, 0, 0 ],
        [ 1, 1, 1, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 1, 1, 1, 1 ]
        ], [
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 0 ],
        [ 1, 0, 0, 0 ],
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 1, 1, 1, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 0 ],
        [ 1, 0, 0, 0 ],
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 1, 1, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 1, 1, 1 ]
        ], [
        [ 1, 1, 1, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 0, 0, 1 ],
        [ 1, 1, 1, 1 ],
        [ 0, 0, 0, 1 ],
        [ 0, 0, 0, 1 ],
        [ 1, 1, 1, 1 ]
            ]
    ];
}

