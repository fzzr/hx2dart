#Hello World

This sample contains a haxe version of the Hello World sample from [dartlang.org/samples](https://www.dartlang.org/samples/) to be compiled to [dart](https://www.dartlang.org/) using [hx2dart][1].

To compile:

1. open build.hxml and replace "$DART_SDK" with the location of the extracted dart sdk.
2. run haxe build.hxml

For comparison the following output is compiled or included in the bin folder:

* **[dart.dart](bin/dart.dart)** : original dart source
* **[hxdart.dart](bin/hxdart.dart)** : dart output compiled by haxe


Each version has a corresponding html file. The Chromium browser included in the dart SDK must be used to view the dart versions. 

*see also [hxjs2dart][2]*

*see also [hxjs2dart Hello World sample](https://bitbucket.org/AndrewVernon/hx2dart/src/fff3a4b4f7c0c82f50f1cc7747d199908c15f1e0/hxjs2dart/samples/helloworld?at=development)*



[1]:https://bitbucket.org/AndrewVernon/hx2dart
[2]:https://bitbucket.org/AndrewVernon/hx2dart/src/deb246ff7c0c1bf35577e500c855f63dc90c9c56/hxjs2dart?at=development