package ;

import dart.html.DivElement;
import dart.html.Element;
import Balls;
import Numbers;

using Math;
using dart.Html;
@:library("dart:html", "dart:math") //TEMP HACK
class Clock
{
    static var fpsAverage:Float;

    public static function main()
    {
        new CountDownClock();
    }

    public static function showFps(fps:Float)
    {
        if (fpsAverage == null) fpsAverage = fps;
        fpsAverage = fps * 0.05 + fpsAverage * 0.95;
        "#notes".query().text = Std.string(fpsAverage.round()) + ' fps';
    }

    public static function makeAbsolute(elem:Element)
    {
        elem.style.left = '0px';
        elem.style.top = '0px';
        elem.style.position = 'absolute';
    }

    public static function makeRelative(elem:Element)
    {
        elem.style.position = 'relative';
    }

    public static function setElementPosition(elem:Element, x:Float, y:Float)
    {
//        elem.style.transform = 'translate(${x}px, ${y}px)';
        elem.style.transform = 'translate(' + Std.string(x) + "px, " + Std.string(y) + 'px)';
    }

    public static function setElementSize(elem:Element, l:Float, t:Float, r:Float, b:Float)
    {
        setElementPosition(elem, l, t);
        elem.style.right = "${r}px";
        elem.style.bottom = "${b}px";
    }
}

class CountDownClock
{
    public static var NUMBER_SPACING = 19.0;
    public static var BALL_WIDTH = 19.0;
    public static var BALL_HEIGHT = 19.0;

    var hours:Array<ClockNumber>;// = new List<ClockNumber>(2);
    var minutes:Array<ClockNumber>;// = new List<ClockNumber>(2);
    var seconds:Array<ClockNumber>;// = new List<ClockNumber>(2);
    var displayedHour:Int;// = -1;
    var displayedMinute:Int;// = -1;
    var displayedSecond:Int;// = -1;
    public var balls:Balls;// = new Balls();

    public function new()
    {
        hours = [null, null];
        minutes = [null, null];
        seconds = [null, null];
        displayedHour = displayedMinute = displayedSecond = -1;
        balls = new Balls();

        var parent = "#canvas-content".query();

        createNumbers(parent, parent.clientWidth, parent.clientHeight);

        updateTime(Date.now());

        Html.window.requestAnimationFrame(tick);
    }

    function tick(time:Float)
    {
        updateTime(Date.now());
        balls.tick(time);
        Html.window.requestAnimationFrame(tick);
    }

    function updateTime(now:Date)
    {
        if (now.getHours() != displayedHour)
        {
            setDigits(pad2(now.getHours()), hours);
            displayedHour = now.getHours();
        }

        if (now.getMinutes() != displayedMinute)
        {
            setDigits(pad2(now.getMinutes()), minutes);
            displayedMinute = now.getMinutes();
        }

        if (now.getSeconds() != displayedSecond)
        {
            setDigits(pad2(now.getSeconds()), seconds);
            displayedSecond = now.getSeconds();
        }
    }

    function setDigits(digits:String, numbers:Array<ClockNumber>)
    {
        for (i in 0 ... numbers.length)
        {
            var digit = digits.charCodeAt(i) - '0'.charCodeAt(0);
            numbers[i].setPixels(ClockNumbers.PIXELS[digit]);
        }
    }

    function pad3(number:Int):String
    {
        if (number < 10)
        {
            return "00${number}";
        }
        if (number < 100)
        {
            return "0${number}";
        }
        return "${number}";
    }

    function pad2(number:Int):String
    {
        if (number < 10)
        {
            return "0${number}";
        }
        return "${number}";
    }

    function createNumbers(parent:Element, width:Float, height:Float)
    {
        var root = new DivElement();
        Clock.makeRelative(root);
        root.style.textAlign = 'center';
        "#canvas-content".query().append(root);          //TODO(av) translate this

        var hSize:Float = (BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING) * 6 + (BALL_WIDTH + NUMBER_SPACING) * 2;
        hSize -= NUMBER_SPACING;

        var vSize:Float = BALL_HEIGHT * ClockNumber.HEIGHT;

        var x:Float = (width - hSize) / 2;
        var y:Float = (height - vSize) / 3;

        for (i in 0 ... hours.length)
        {
            hours[i] = new ClockNumber(this, x, Balls.BLUE_BALL_INDEX);
            root.append(hours[i].root);
            Clock.setElementPosition(hours[i].root, x, y);
            x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
        }

        root.append(new Colon(x, y).root);
        x += BALL_WIDTH + NUMBER_SPACING;

        for (i in 0 ... minutes.length)
        {
            minutes[i] = new ClockNumber(this, x, Balls.RED_BALL_INDEX);
            root.append(minutes[i].root);
            Clock.setElementPosition(minutes[i].root, x, y);
            x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
        }

        root.append(new Colon(x, y).root);
        x += BALL_WIDTH + NUMBER_SPACING;

        for (i in 0 ... seconds.length)
        {
            seconds[i] = new ClockNumber(this, x, Balls.GREEN_BALL_INDEX);
            root.append(seconds[i].root);
            Clock.setElementPosition(seconds[i].root, x, y);
            x += BALL_WIDTH * ClockNumber.WIDTH + NUMBER_SPACING;
        }
    }
}
